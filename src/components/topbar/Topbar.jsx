import './topbar.scss';
import PersonIcon from '@mui/icons-material/Person';
import MailIcon from '@mui/icons-material/Mail';
import { useState } from "react";

function Topbar() {

  const [menuOpen, setMenuOpen] = useState(false);

  const toggleMenu = () => {
    setMenuOpen(!menuOpen);
  }

  return (
    <div className={"topbar " + (menuOpen && "active")} >
      <div className="wrapper">
        <div className="left">
          <a href="#intro" className='logo'>John.</a>
          <div className="item-container">
            <PersonIcon className="icon"/>
            <span>+996 555 00 11 22</span>
          </div>

          <div className="item-container">
            <MailIcon className="icon" />
            <span>john@john.com</span>
          </div>
        </div>
        <div className="right">
          <div className="hamburger" onClick={()=>toggleMenu()}>
            <span className='line-1'></span>
            <span className='line-2'></span>
            <span className='line-3'></span>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Topbar
